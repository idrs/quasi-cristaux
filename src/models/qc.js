//import jsonFileUrl from '../../table.json'
//import jsonCatFileUrl from '../../categories.json'
//import jsonEtudeFileUrl from '../../etudeFormatee.json'

const jsonFileUrl = './table.json'
const jsonCatFileUrl = './categories.json'
const jsonEtudeFileUrl = './etudeFormatee.json'

var _ = require('lodash');

import Fuse from 'fuse.js'

var m = require("mithril");
//var fs = require("file-saver");

var versionCheck = 1;

function storageAvailable(type) {
    var storage;
    try {
        storage = window[type];
        var x = '__storage_test__';
        storage.setItem(x, x);
        storage.removeItem(x);
        return true;
    }
    catch (e) {
        return e instanceof DOMException && (
            // everything except Firefox
            e.code === 22 ||
            // Firefox
            e.code === 1014 ||
            // test name field too, because code might not be present
            // everything except Firefox
            e.name === 'QuotaExceededError' ||
            // Firefox
            e.name === 'NS_ERROR_DOM_QUOTA_REACHED') &&
            // acknowledge QuotaExceededError only if there's something already stored
            (storage && storage.length !== 0);
    }
}

var reloadEveryTime = false; //debug

var QC = {
    store: {},
    loadedStore: false,
    loadStore: function () {
        if (storageAvailable('localStorage')) {
            // Yippee! We can use localStorage 
            if (!localStorage.getItem('quasi-cristaux') || reloadEveryTime || QC.store === null) {
                return m.request(jsonFileUrl).then(function (storeJSON) {
                    console.log("loadingStore", storeJSON);
                    if (storeJSON) {
                        localStorage.setItem('quasi-cristaux', JSON.stringify(storeJSON));
                        QC.store = (storeJSON);
                        QC.loadedStore = true;
                        QC.initSearch();
                        console.log("Store loaded");
                        //console.log(QC.store['1998-01']);
                        m.redraw();
                    }
                    else {
                        console.log("Get null ???")
                    }
                }, function (err) { console.log(err) });
            } else {
                QC.store = JSON.parse(localStorage.getItem('quasi-cristaux'));
                QC.initSearch();
                QC.loadedStore = true;
            }
        }
        else {
            if (!QC.store.hasOwnProperty('1801-00')) {
                return m.request(jsonFileUrl).then(function (storeJSON) {
                    console.log("loadingStore");
                    QC.store = (storeJSON);
                    QC.loadedStore = true;
                QC.initSearch();

                    console.log("Store loaded");
                });
            }
        }
    },
    etude: [],
    loadedEtude: false,
    loadEtude: function () {
        if (storageAvailable('localStorage')) {
            // Yippee! We can use localStorage 
            if (!localStorage.getItem('quasi-cristaux-etude') || reloadEveryTime || QC.etude === null) {
                return m.request(jsonEtudeFileUrl).then(function (storeJSON) {
                    console.log("loadingEtude", storeJSON);
                    if (storeJSON) {
                        localStorage.setItem('quasi-cristaux-etude', JSON.stringify(storeJSON));
                        QC.etude = (storeJSON);
                        QC.loadedEtude = true;
                    }
                    else {
                        console.log("Get null ???")
                    }
                }, function (err) { console.log(err) });
            } else {
                QC.etude = JSON.parse(localStorage.getItem('quasi-cristaux-etude'));
                QC.loadedEtude = true;

            }
        }
        else {
            if (!QC.store.hasOwnProperty('1801-00')) {
                return m.request(jsonEtudeFileUrl).then(function (storeJSON) {
                    console.log("loadingEtude");
                    QC.etude = JSON.parse(storeJSON);
                    QC.loadedEtude = true;
                    console.log("Etude loaded");
                });
            }
        }
    },
    reloading: false,
    categories: [],
    loadedCat: false,
    loadCat: function () {
        if (storageAvailable('localStorage')) {
            // Yippee! We can use localStorage 
            //console.log("1: ",!localStorage.getItem('quasi-cristaux-cat'),"2: ", reloadEveryTime,"3: ", QC.categories === null)
            if (!localStorage.getItem('quasi-cristaux-cat') || reloadEveryTime || QC.categories === null) {
                return m.request({ method: "GET", url: jsonCatFileUrl }).then(function (storeJSON) {
                    console.log("loadingCat", storeJSON);
                    localStorage.setItem('quasi-cristaux-cat', JSON.stringify(storeJSON));
                    QC.categories = (storeJSON).content;
                    console.log("Categories loaded: ", QC.categories);
                    QC.loadedCat = true;
                    QC.loadEtude();
                    QC.loadStore();
                    //console.log(QC.store['1998-01'])
                });
            } else {

                var storedCat = JSON.parse(localStorage.getItem('quasi-cristaux-cat'));
                //console.log("version ",versionCheck,"| stored: ")
                if (storedCat.version !== versionCheck) {
                    console.log("change version")
                    localStorage.removeItem("quasi-cristaux");
                    localStorage.removeItem("quasi-cristaux-cat");
                    localStorage.removeItem("quasi-cristaux-etude");
                    QC.loadedStore = false;
                    QC.reloading = true;
                    m.redraw();
                    QC.loadCat();
                }
                else {
                    QC.reloading = false;

                    QC.categories = storedCat.content;
                    QC.loadEtude();
                    QC.loadStore();
                }
                QC.loadedCat = true;

            }
        }
        else {
            if (!QC.categories[0].hasOwnProperty('nom')) {
                return m.request(jsonCatFileUrl).then(function (storeJSON) {
                    console.log("loadingCategories");
                    QC.categories = JSON.parse(storeJSON).content;
                    QC.loadedCat = true;
                    QC.loadEtude();
                    QC.loadStore();
                    console.log("Categories loaded");
                });
            }
        }
    },
    initSearch: function(){
        const options = {
            includeScore: true,
            // equivalent to `keys: [['author', 'tags', 'value']]`
            keys: ['contenu']
        }
        QC.chercheFun = new Fuse(Object.values(QC.store).concat(QC.etude), options);
    },
    isUnloaded: function () {
        //console.log("Test de la présence des données:")
        var booleen = !QC.loadedStore || !QC.loadedCat || !QC.loadedEtude
        //console.log("store",QC.loadedStore,"etude",QC.loadedEtude,"cat",QC.loadedCat,"total",booleen)
        return booleen
    },
    sonnet: {},
    selectionne: "",
    allerSonnet: function (id) {
        m.route.set("/sonnet/:id", { id: id });
    },
    aleatoire: function () {
        var keys = Object.keys(QC.store);
        var l = keys.length;
        var i = Math.floor(Math.random() * l);
        return keys[i];
    },
    chercheFun:null,
    cherche: function(str){
        var res = QC.chercheFun.search(str);
        return res;
    },
    filtre: function (cat) {
        if (typeof (cat) === 'string') {
            cat = [cat];
        }
        var valeurs = Object.values(QC.store).sort((a, b) => a.nom < b.nom ? -1 : 1);

        console.log(valeurs[1]);
        console.log("testing filter")
        var filtree = valeurs.filter((elt) => elt.tags.filter((t) => cat.includes(t)).length > 0);
        console.log(filtree);
        return filtree;
    },
    annee: function (nom) {
        return (nom.slice(0, 4));
    },
    auteur: function (sonnet) {
        var aut = sonnet.contenu.match(/__([^_]*)__/);
        return aut ? aut[1] : "";
    },
    sortById: function (a, b) {
        var nameA = a.id;
        var nameB = b.id;
        if (nameA < nameB) {
            return -1;
        }
        if (nameA > nameB) {
            return 1;
        }

        // names must be equal
        return 0;
    }
}


QC.loadCat();


module.exports = QC
