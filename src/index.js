var m = require("mithril");
var Layout = require("./views/Layout")
var QC = require("./models/qc")
var Accueil = require("./views/Accueil")
var Sonnet = require("./views/Sonnet")
var Etude = require("./views/Etude")
var Chapitre = require("./views/Chapitre")

var Periode = require("./views/Periode")

var Aleatoire = require("./views/Aleatoire")
var Categorie = require("./views/Categorie")
var Cherche = require("./views/Cherche")
var Categories = require("./views/Categories")

import './mystyles.scss';



m.route(document.getElementById("app"), "/accueil", {
    "/accueil": {
        render: function () {
            return m(Layout, m(Accueil))
        }
    },
    "/categories": {
        render: function () {
            return m(Layout, m(Categories))
        }
    },
    "/sonnet/:id": {
        render: function (vnode) {
            return m(Layout, m(Sonnet, vnode.attrs))
        }
    },
    "/aleatoire": {
        render: function () {
            return m(Layout, m(Aleatoire))
        }
    },
    "/categorie/:cat": {
        render: function (vnode) {
            console.log("vnode", vnode)
            return m(Layout, m(Categorie, vnode.attrs))
        }
    },
    "/etude/:id": {
        render: function (vnode) {
            return m(Layout, m(Etude, vnode.attrs))
        }
    },
    "/chapitre/:id": {
        render: function (vnode) {
            return m(Layout, m(Chapitre, vnode.attrs))
        }
    },
    "/periode/:id": {
        render: function (vnode) {
            return m(Layout, m(Periode, vnode.attrs))
        }
    },

    "/cherche/:cherche": {
        render: function (vnode) {
            return m(Layout, m(Cherche, vnode.attrs))
        }
    },
})

