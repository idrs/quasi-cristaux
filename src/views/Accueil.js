var m = require("mithril");
//import marked from "marked";
var QC = require("../models/qc");
//var Aleatoire = require("./Aleatoire");
var Avertissement = require("./Avertissement");
var marked = require('marked')

function Accueil() {
    //var drawing = 0;
    var sonnet=QC.store[QC.aleatoire()];
    return {
        //oninit: function(){
        //    QC.loadStore();
        //},
        
        view: function () {
            //drawing++;
            //console.log("accueil chargé...",drawing) {id:QC.aleatoire()}
            return m("#accueil", m("div",m('.section',m(".title.is-4","Au hasard"),m("#main.box",
            m("ul#tags.block",sonnet.tags.map((t)=>
                m("li.tag.ml-1",{onclick:(e)=>m.route.set("/categorie/:tag",{tag:t})},t))),
            m(".title.is-5",sonnet["titre"]),
            m(".right",QC.annee(sonnet['nom'])),
            m(".content",m.trust(marked(sonnet["contenu"])))
            ))), 
                m(".section",m(Avertissement)),
/*                 m(".section",
                m("#liste",
                    Object.values(QC.store).sort((a,b)=>a.nom<b.nom?-1:1)
                    .map(sonnet => m(".box",
                    { onclick: () => m.route.set("/sonnet/:id", { id: sonnet["nom"] }) },
                    m("p.title.is-6", m("span.bold", sonnet["titre"]),
                        m("span.right", QC.annee(sonnet['nom']))),
                    m("#2nde",
                        m("span.auteur.subtitle.is-6", QC.auteur(sonnet))),
                    m("p.tags", sonnet.tags.join(", "))

                )
                
                 )
                        )) */

                    //m(".column.is-one-quarter",m(Etiquette))
                );
        },
    }
}

module.exports = Accueil;