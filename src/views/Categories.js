var m = require( "mithril");
//import marked from "marked";
var QC  = require("../models/qc");


function arrayToCatList(cl,lvl){
    if(!lvl){lvl=0;}
    return m("ul.content",
                cl.map(
                    (e)=>m("li.l"+lvl+(lvl===0?".box":""),
                        
                        m("p.bold",{onclick:()=>m.route.set('/categorie/:cat',{cat:e.nom})},e.nom),
                        e.enfant?arrayToCatList(e.enfant,lvl+1):null)
                    )
    );
}

function Depliable (vnode) {
    console.log('nom',vnode.attrs.nom,' et liste ',vnode.attrs.liste)
    var deplie = false;
    var disp = "";
    if(!vnode.attrs.liste){
        console.log('disp should be none')
        disp = ".is-hidden";
    }
    console.log('disp:',disp)
    function toggle(){
        deplie = !deplie;
    }
    return {
        view: function(vnode){
            return [m('.level.is-mobile.mb-1',
                m('.level-left',m(".level-item",{onclick:()=>m.route.set('/categorie/:cat',{cat:vnode.attrs.nom})},vnode.attrs.nom),
                m(".icon-pli.level-item"+disp,{onclick:()=>toggle()},deplie?'-':'+'))),
            deplie?m('.pl-4',vnode.attrs.liste.map((l)=>m(Depliable,{nom:l.nom,liste:l.enfant}))):null]
        }
    }
}


function Categories(){
    //var drawing = 0;
    //var catSet = new Set(Object.values(QC.store)
    //    .map(sonnet=>sonnet['tags'])
    //    .flat());
    //var catArray = Array.from(catSet).sort();    
    return {
        //oninit: function(){
        //    QC.loadStore();
        //},
        view: function(){
            //drawing++;
            //console.log("accueil chargé...",drawing)
            return m("#liste.section.columns.is-multiline.is-centered",QC.categories.map((c)=>m(".column.is-one-third.",m(".box",m(Depliable,{nom:c.nom,liste:c.enfant}))))
            );
        },
    }
}

module.exports = Categories;