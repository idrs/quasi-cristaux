var m = require("mithril");
//import marked from "marked";
var QC = require("../models/qc");
var Aleatoire = require("./Aleatoire");
var Avertissement = require("./Avertissement");


function Accueil() {
    //var drawing = 0;
    return {
        //oninit: function(){
        //    QC.loadStore();
        //},
        view: function () {
            //drawing++;
            //console.log("accueil chargé...",drawing)
            return m("#accueil", m(Aleatoire), m(Avertissement),
                m("#liste",
                    Object.values(QC.store)
                        .map(sonnet => m(".box",
                            { onclick: () => m.route.set("/sonnet/:id", { id: sonnet["nom"] }) },
                            m("p.bold", sonnet["titre"]),
                            m(".right", QC.annee(sonnet['nom']))
                        )
                        )
                        
                    //m(".column.is-one-quarter",m(Etiquette))
                ));
        },
    }
}

module.exports = Accueil;