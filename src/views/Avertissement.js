var m = require( "mithril");
//import marked from "marked";
var QC  = require("../models/qc");


function Avertissement(){
    return {
        view: function(){ 
            return m("#avert.content",
                m.trust(QC.etude.filter((e)=>e.id==="0-0")[0].contenu)
                );
        },
    }
}

module.exports = Avertissement;
