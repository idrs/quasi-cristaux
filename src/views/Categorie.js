var m = require( "mithril");
//import marked from "marked";
var QC  = require("../models/qc");

function tagsFlattener(list){
    let tagFlatList = [];
    for(let l of list){
        tagFlatList.push(l.nom);
        if(l.enfant){
            Array.prototype.push.apply(tagFlatList,tagsFlattener(l.enfant));
        }
    }
    return tagFlatList;
}

function tagsFinder(tag,list){
    let tagList = [];
    for(let t of list){
        if(t.nom===tag){
            tagList.push(tag)
            if(t.enfant){
                Array.prototype.push.apply(tagList,tagsFlattener(t.enfant));
            }
        }
        else {
            if(t.enfant){
                let tEnfant = tagsFinder(tag,t.enfant);
                Array.prototype.push.apply(tagList,tEnfant);
            }
        }
    }
    return tagList;
}

function tagAscent(tag,list){
    let ascent = [];
    for(let t of list){
        if(t.nom===tag){
            ascent.push(tag)
        }
        else{
            if(t.enfant){
                let res = tagAscent(tag,t.enfant);
                if(res.length>0){
                    res.unshift(t.nom);
                    return res;
                }
            }
        
        }
    }
    return ascent;
}

function Categorie(){
    //var drawing = 0;
    return {
        //oninit: function(){
        //    QC.loadStore();
        //},
        view: function(vnode){
            //console.log("inside cat, vnode",vnode)
            //console.log(vnode.attrs.cat,QC.filtre(vnode.attrs.cat));
            //drawing++;
            //console.log("accueil chargé...",drawing)
            var ascendance = tagAscent(vnode.attrs.cat,QC.categories);
            return m("#liste.section",
                    m('.breadcrumb.is-centered',{'aria-label':'breadcrumbs'},
                        m('ul',
                        ascendance.map((a,idx)=>m('li'+(idx===(ascendance.length-1)?'.is-active':''),m('a',{onclick:()=>m.route.set('/categorie/:cat',{cat:a})},a))))),
                    QC.filtre(tagsFinder(vnode.attrs.cat,QC.categories))
                    .map(sonnet => m(".box",
                    { onclick: () => m.route.set("/sonnet/:id", { id: sonnet["nom"] }) },
                    m("p.title.is-6", m("span.bold", sonnet["titre"]),
                        m("span.right", QC.annee(sonnet['nom']))),
                    m("#2nde",
                        m("span.auteur.subtitle.is-6", QC.auteur(sonnet))),
                    m("p.tags", sonnet.tags.join(", "))

                )
                ) 
                
                    //m(".column.is-one-quarter",m(Etiquette))
            );
        },
    }
}

module.exports = Categorie;