var m = require("mithril");
//import marked from "marked";
var QC = require("../models/qc");

function tagsFlattener(list) {
    let tagFlatList = [];
    for (let l of list) {
        tagFlatList.push(l.nom);
        if (l.enfant) {
            Array.prototype.push.apply(tagFlatList, tagsFlattener(l.enfant));
        }
    }
    return tagFlatList;
}

function periodeFinder(e, per) {
    //console.log(e," ",per)
    let decomp = per.match(/(\d{4})-(\d{4})/);
    let deb = decomp[1];
    let fin = decomp[2];
    let annee = e.nom.match(/(\d{4})|(coda)/)[0];
    //onsole.log('nom:',e.nom,' per:',per,' annee:',annee,' deb',deb);
    return (annee >= deb && annee <= fin) || (annee === "coda" && deb === "1999")
};



function Periode() {
    //var drawing = 0;
    console.log('periode')
    return {
        //oninit: function(){
        //    QC.loadStore();
        //},
        view: function (vnode) {
            //console.log("inside cat, vnode",vnode)
            //console.log(vnode.attrs.cat,QC.filtre(vnode.attrs.cat));
            //drawing++;
            //console.log("accueil chargé...",drawing)
            //console.log('periode id:', vnode.attrs.id);
            return m("#liste", m('h3', vnode.attrs.id),
                 Object.values(QC.store).sort((a,b)=>a.nom<b.nom?-1:1)
                    .filter((e) => periodeFinder(e, vnode.attrs.id))
                    .map(sonnet => m(".box",
                        { onclick: () => m.route.set("/sonnet/:id", { id: sonnet["nom"] }) },
                        m("p.title.is-6", m("span.bold", sonnet["titre"]),
                            m("span.right", QC.annee(sonnet['nom']))),
                        m("#2nde",
                            m("span.auteur.subtitle.is-6", QC.auteur(sonnet))),
                        m("p.tags", sonnet.tags.join(", "))

                    )
                    ) 

                //m(".column.is-one-quarter",m(Etiquette))
            );
        },
    }
}

module.exports = Periode;