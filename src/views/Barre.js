var m = require("mithril");
var QC = require("../models/qc");

import imgURL from "../assets/android-chrome-192x192.png";



function Barre() {
    var dropped = false;
    var searchString = "";
    function toggle() {
        dropped = !dropped;
        console.log(dropped);
        m.redraw();
    }
    function chapsEtude(part) {
        return Object.values(QC.etude)
            .filter((e) => e.part === Number(part))
            .sort(QC.sortById)
            .map(chap => m("a.navbar-item", { onclick: () =>  {toggle();m.route.set("/chapitre/:id", { id: chap.id }) }}, chap["titre"]))
    }
    return {
        view: function (vnode) {
            //console.log('Barre')
            //console.log(vnode.attrs)
            //console.log("search: ",searchString)
            //console.log("redraw")
            return m("nav.navbar#barre.is-fixed-top.is-dark", {},
                m(".navbar-brand",
                    m(".navbar-item", { onclick: (e) => m.route.set("/accueil") },
                        m("img.image.m-2", { src: imgURL }),
                        m('.container',
                            m(".is-size-5.has-text-weight-semibold", "Quasi-cristaux"),
                            m(".is-size-7.is-italic.has-text-centered", "par Jacques Roubaud")
                        )
                    ),
                    m('.navbar-burger' + (dropped ? '.is-active' : ''),
                        { role: "button", "aria-label": "menu", "aria-expanded": "false", "data-target": "navbarElements", onclick: toggle },
                        m("span", { "aria-hidden": "true" }, ""),
                        m("span", { "aria-hidden": "true" }, ""),
                        m("span", { "aria-hidden": "true" }, ""))),
                m("#navbarElements.navbar-menu" + (dropped ? '.is-active' : ''),
                    m(".navbar-start.is-centered",
                        m("a.navbar-item.is-mobile",
                            { onclick: (e) => { toggle(); m.route.set("/aleatoire") } },
                            m("ion-icon",{name:"dice"})),
                        m("a.navbar-item.is-mobile",
                            { onclick: (e) => { toggle(); m.route.set("/categories") } }, m("ion-icon",{name:"pricetag"})),
                        m(".navbar-item.is-mobile.field",m("p.control.has-icons-left.is-dark",{ onclick: (e) => { toggle(); m.route.set("/cherche/"+searchString) } },
                             m("input.is-dark",{type: "text", placeholder:"Recherche",onkeydown:(e)=>{(e.keyCode === 13 )?m.route.set("/cherche/"+searchString):(e.redraw = false) } ,oninput:(e)=>{searchString=e.target.value}}),
                             m("span.icon.is-small.is-left",m("ion-icon",{name:"search"})))
                            )
                        ),
                    m('.navbar-end',
                        m("a.navbar-item.has-dropdown.is-hoverable",
                            m("a.navbar-link",
                                { onclick: (e) => { toggle(); m.route.set("/etude/:id", { id: 1 }) } },
                                "I. Choix de sonnets"),
                            m(".navbar-dropdown", chapsEtude(1) ) 
                        ),
                        m("a.navbar-item.has-dropdown.is-hoverable",
                            m("a.navbar-link",
                                { onclick: (e) => { toggle(); m.route.set("/etude/:id", { id: 2 }) } },
                                "II. Éléments d'histoire"),
                            m(".navbar-dropdown", chapsEtude( 2))
                        ),
                        m("a.navbar-item.has-dropdown.is-hoverable",
                            m("a.navbar-link",
                                { onclick: (e) => { toggle(); m.route.set("/etude/:id", { id: 3 }) } },
                                "III. Description du sonnet français (1801-1998)"),
                            m(".navbar-dropdown.is-right", chapsEtude(3))
                        )
                    )
                )

            )      
        }
    }
}



module.exports = Barre;
