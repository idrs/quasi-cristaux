var m = require("mithril");
//import marked from "marked";
var QC = require("../models/qc");

function Chapitre() {
    //var drawing = 0;
    return {
        //oninit: function(){
        //    QC.loadStore();
        //},
        view: function (vnode) {
            //drawing++;
            //console.log("accueil chargé...",drawing)
            var chap = QC.etude.filter((e)=>e.id===vnode.attrs.id)[0];
            //console.log("chap",chap)
            return m("#chapitre.section.content",
                    m.trust(chap.contenu)
                    //m(".column.is-one-quarter",m(Etiquette))
                );
        },
    }
}

module.exports = Chapitre;