var m = require("mithril");
//import marked from "marked";
var QC = require("../models/qc");
//var Aleatoire = require("./Aleatoire");
//var Avertissement = require("./Avertissement");
var ListeChap = require("./ListeChap")



function Etude() {
    //var drawing = 0;
    return {
        //oninit: function(){
        //    QC.loadStore();
        //},
        view: function (vnode) {
            //drawing++;
            //console.log("accueil chargé...",drawing)
            //console.log("liste etude",Object.values(QC.etude).map((e)=>e.part+"/"+e.id+"/"+e.titre));
            //console.log(vnode.attrs.id);
            //console.log(Object.values(QC.etude).filter((e)=>e.part===vnode.attrs.id).map((e)=>e.titre+'\n'))
            // console.log('liste des chapitres',Object.values(QC.store)
            // .filter((e)=>e.titre.match("hapitre"))
            // .map(chap=>chap.titre+" "+chap.nom));
            //vnode.attrs.id==="1"?console.log('listeChap'):null
            return m("#etude.section",
                    Object.values(QC.etude)
                        .filter((e)=>e.part===Number(vnode.attrs.id))
                        .sort(QC.sortById)
                        .map(chap => m(".box",
                            { onclick: () => m.route.set("/chapitre/:id", { id: chap.id }) },
                            m("p.bold.is-title", chap["titre"]),
                        )
                        ),
                    (vnode.attrs.id==="1"?m(ListeChap):null)
                    //m(".column.is-one-quarter",m(Etiquette))
                );
        },
    }
}

module.exports = Etude;