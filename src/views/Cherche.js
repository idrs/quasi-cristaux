var m = require( "mithril");
//import marked from "marked";
var QC  = require("../models/qc");

function extract(res){
    if(res.hasOwnProperty('id')){
        //type étude
        return  m(".box",
                            { onclick: () => m.route.set("/chapitre/:id", { id: res.id }) },
                            m("p.bold.is-title.is-6", res["titre"]));
    }
    else{
        //type sonnet
        //console.log("in extract, no id: ",res)
        return m(".box",
                    { onclick: () => m.route.set("/sonnet/:id", { id: res["nom"] }) },
                    m("p.title.is-6", m("span.bold", res["titre"]),
                        m("span.right", QC.annee(res['nom']))),
                    m("#2nde",
                        m("span.auteur.subtitle.is-6", QC.auteur(res))),
                    m("p.tags", res.tags.join(", "))
                );
    }
}

function Cherche(){
    //var drawing = 0;
    return {
        //oninit: function(){
        //    QC.loadStore();
        //},
        view: function(vnode){
            //console.log("inside cat, vnode",vnode)
            //console.log(vnode.attrs.cat,QC.filtre(vnode.attrs.cat));
            //drawing++;
            //console.log("accueil chargé...",drawing)
            var res = QC.cherche(vnode.attrs.cherche);
            //console.log(vnode.attrs.cherche);
            return m("#liste.section",
                    res
                    .map(resultat => extract(resultat.item))
                 
                
                    //m(".column.is-one-quarter",m(Etiquette))
            );
        },
    }
}

module.exports = Cherche;

