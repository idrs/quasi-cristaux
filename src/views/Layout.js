var m = require( "mithril");
var Barre = require("./Barre");
var QC = require("../models/qc")
var Charge = require('./Charge')


function Layout() {
    return {
    view: function(vnode) {
        return QC.isUnloaded()?m("main.container",m(Charge)):m("#layout.has-navbar-fixed-top",
            m(Barre,vnode.attrs),
            m("main.has-navbar-fixed-top.container.is-max-desktop",vnode.children)
        )
    }
}
}

module.exports = Layout
