var m = require( "mithril");
//import marked from "marked";
var QC  = require("../models/qc");
var Sonnet = require("./Sonnet")


function Aleatoire(){
    return {
        view: function(){ 
            m.route.set('/sonnet/:id',{id:QC.aleatoire()});
        }
    }
}

module.exports = Aleatoire;
