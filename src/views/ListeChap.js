var m = require("mithril");
//import marked from "marked";
var QC = require("../models/qc");
//var Aleatoire = require("./Aleatoire");
//var Avertissement = require("./Avertissement");



function sortChap(a, b) {
    var nameA = a.titre.match(/hapitre\s(\d+)/)[1].padStart(2, "0");
    var nameB = b.titre.match(/hapitre\s(\d+)/)[1].padStart(2, "0");
    if (nameA < nameB) {
        return -1;
    }
    if (nameA > nameB) {
        return 1;
    }

    // names must be equal
    return 0;
}


function ListeChapitres() {
    return {
        view: function(){
            return m('#listeChap',Object.values(QC.store)
            .filter((e) => e.titre.match("hapitre"))
            .sort(sortChap)
            .map(chap => m(".box",
                { onclick: () => m.route.set("/periode/:id", { id: chap.nom }) },
                m("p.bold", chap["titre"]),
            )
            ))
        }
    }
}
module.exports = ListeChapitres;