var m = require( "mithril");
//import marked from "marked";
var QC  = require("../models/qc");
var marked = require("marked");



  const renderer = {
      paragraph(text) {
          const match = text.split('<br>')
          //console.log(match);
          var html="";
          if(match.length>0){
              html += '<p>'
              for(var txt of match){
                  html += `<p class='verse mb-0'>${txt}</p>`
              }
              html += '<\p>';
          }
          else{
              if(text.trim()!==''){
                  html = `<p>${text}<\p>`;
              }
          }
          //console.log(html)
          return html;
      }
    };
    

marked.use({renderer}); 

function Sonnet(){
    return {
        view: function(vnode){ 
            var sonnet=QC.store[vnode.attrs.id];
            //console.log(vnode.attrs,sonnet)
            return m('.section',m("#main.box",
                m("ul#tags.block",sonnet.tags.map((t)=>
                    m("li.tag.ml-1",{onclick:(e)=>m.route.set("/categorie/:tag",{tag:t})},t))),
                m(".title.is-5",sonnet["titre"]),
                m(".right",QC.annee(sonnet['nom'])),
                m(".content",m.trust(marked(sonnet["contenu"])))
                ));
        },
    }
}

module.exports = Sonnet;
