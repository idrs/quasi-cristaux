var fs =require('fs');

function isIterable(obj) {
    // checks for null and undefined
    if (obj == null) {
      return false;
    }
    return typeof obj[Symbol.iterator] === 'function';
  }
  

function categoriesFromNav(nav,indent){
    if(!indent){indent="";}
    var liste = nav.ul.li;
    var categories = []
    console.log(indent,"type of liste:",typeof(liste));
    if(nav.a){console.log(indent,nav.a["#text"]);}
    if(isIterable(liste)){
        for(let li of liste){
            let categorie = {}
            categorie.nom = li.a["#text"].replace(/–/g,'-');
            console.log(indent,categorie.nom);
            if(li.ul){
                categorie.enfant = categoriesFromNav(li,indent+"  ");
            }
            categories.push(categorie);
        }
    }
    else{
        categories.push({nom:liste.a["#text"]});
    }
    return categories;
}


var dirname = '/home/sylvain/Devel/Quasi-cristaux/' 

fs.readFile(dirname+'nav.json',(err,data)=>{
    if(err){console.log(err);}
    else{
        var nav=JSON.parse(data)
        console.log("nav",nav);

        var test = categoriesFromNav(nav);

        console.log(test)

        fs.writeFile(dirname+'categories.json',JSON.stringify(test),(err)=>{console.log("Erreur dans l'écriture:",err)});
    }
});

