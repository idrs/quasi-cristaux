const fs = require('fs');


function whatPart(e) {
    let parts = ["avert", "001", "002", "003"];
    for (var i = 0; i < 4; i++) {
        if (e[parts[i]]) { return i; }
    }
}

function transfo(e) {
    var p = whatPart(e);
    var ps = JSON.stringify(p).padStart(3,'0');
    var chap = e[p===0?"avert":ps].match(/apitre\s(\d)/);
    return {
        id: p+"-"+(chap?chap[1]:""),
        part: p,
        titre: e[p===0?"avert":ps],
        contenu: e.contenu,
    }
}

//console.log(transfo({"001":"yo",contenu:"yoyo"}))

fs.readFile('./etude.json', 'utf8', (err, data) => {

    if (err) {
        console.log(`Error reading file from disk: ${err}`);
    } else {

        // parse JSON string to JSON object
        const databases = JSON.parse(data);
        console.log(databases.length)
        // print all databases
        var result = databases.map(db => {
            console.log(db["web-scraper-order"]);
            return transfo(db);
        });

        fs.writeFile('./etudeFormatee.json',JSON.stringify(result),(err)=>console.log(err));
    }

});
