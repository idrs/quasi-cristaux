import csv
from html2markdown import convert
import re
import json

html_write = False

with open('roubaud.csv', mode='r') as csv_file:
    csv_reader = csv.DictReader(csv_file)
    line_count = 0
    old = ""
    tags = []
    index = {}
    articles = {}
    for row in csv_reader:
        article = {}
        if line_count == 0:
            print(f'Column names are {", ".join(row)}')
            line_count += 1
        #print(f'\t{row["auteur"]} a écrit {row["titre"]}.')
        else:
            lien = row["articles-href"]

            titre = re.search("[^—]*",row["titre"])

            fichier = "<h3>"+titre[0]+"</h3>\n"

            nom = re.split("/",lien)[-2]
            #print(nom)
            inter = re.search("incise|chapitre",nom)
            if inter:
                supp = inter[0]
                if inter == "chapitre":
                    fichier = "<h2>"+row["titre"]+"</h2>\n"
            else:
                supp = ""

            recherche = re.search("(\d{4}|coda)[-\d+]*",nom)
            if not(recherche):

                if nom=="205":
                    recherche = ["1814-2"]
                elif nom=="194-16":
                    recherche = ["1944-16"]
                elif nom=="967":
                    recherche = ["1842-2"]
                elif nom=="565":
                    recherche = ["1832-7"]
                elif nom=="chapitre-4":
                    recherche = ["1845-"]
                    supp = "chapitre"
                elif nom=="code-49":
                    recherche=["coda-49"]
                else:
                    print(row)
                    input()
            annee_indice = re.split(r"-",recherche[0])
            if len(annee_indice)>1:
                nom = annee_indice[0]+"-"+annee_indice[1].zfill(2)
            else:
                nom = annee_indice[0]+"-00"
            #print(convert(row['texte']))
            tag = row['tags']
            if tag in index:
                index[tag].append(nom)
            else:
                index[tag]=[nom]
            tag_link = "<a href='index.html#"+tag+"'>"+tag+"</a>"
            if old==nom:
                tags.append(tag)
                tags_link.append(tag_link)
            else:
                tags = [tag]
                tags_link = [tag_link]
            old = nom
            fichier += '<p style="font-style:italic;font-size:0.8em;text-align:right">'+", ".join(sorted(tags_link))+'</p>'
            fichier += row["texte"]
            article["nom"]=nom
            article["titre"]=titre[0]
            article["contenu"]=row["texte"]
            article["tags"]=sorted(tags)
            articles[nom]=article
            if html_write:
                with open("roubaud/"+nom+supp+".html",mode='w') as html:
                    html.write(fichier)
                    line_count += 1
    print("nom",type(nom))
    print("titre",type(titre[0]))
    print("tags",type(tags))
    print("contenu",type(fichier))
    with open("table.json",mode='w') as jsonFile:
        json.dump(articles,jsonFile)

    print(f'Processed {line_count} lines.')
